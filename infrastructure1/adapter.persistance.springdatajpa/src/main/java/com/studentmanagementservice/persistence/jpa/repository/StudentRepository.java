package com.studentmanagementservice.persistence.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.studentmanagementservice.persistence.jpa.entity.StudentEntity;



public interface StudentRepository extends JpaRepository<StudentEntity, Integer>{
	 StudentEntity findByStudentId(Integer studentId);
}
