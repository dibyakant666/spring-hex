package com.studentmanagementservice.infrastructure.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.studentmanagementservice.app.service.api.StudentService;
import com.studentmanagementservice.domain1.model.Student;



@RestController
public class StudentControllerImpl implements StudentController {
	private StudentService studentService;

    @Autowired
    public StudentControllerImpl(StudentService studentService) {
        this.studentService = studentService;
    }


    public ResponseEntity<List<Student>> getStudents() {
        return new ResponseEntity<List<Student>>(studentService.getStudents(), HttpStatus.OK);
    }

  
    public ResponseEntity<Student> addStudent(Student student) {
    	studentService.addStudent(student);
        return new ResponseEntity<Student>(HttpStatus.OK);
    }

 
    public ResponseEntity<Void> removeStudent(Student student) {
    	studentService.removeStudent(student);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

  
    public ResponseEntity<Student> getStudentById(Integer studentId) {
        return new ResponseEntity<Student>(studentService.getStudentById(studentId), HttpStatus.OK);
    }


	

	
	

}
