package com.studentmanagementservice.infrastructure.graphql.adapter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.studentmanagementservice.domain1.model.Student;
import com.studentmanagementservice.domain1.spi.StudentPersistencePort;
import com.studentmanagementservice.persistence.jpa.entity.StudentEntity;
import com.studentmanagementservice.persistence.jpa.repository.StudentRepository;

import graphql.kickstart.tools.GraphQLResolver;

@Component
public class Resolver implements GraphQLResolver<Student>{
	private StudentRepository studentRepository;
	
	@Autowired
	public Resolver(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	public StudentEntity getStudent(int id) {
		return studentRepository.getById(id);
	}

}
