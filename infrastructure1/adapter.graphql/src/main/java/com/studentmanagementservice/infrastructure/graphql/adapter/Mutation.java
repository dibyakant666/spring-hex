package com.studentmanagementservice.infrastructure.graphql.adapter;

import graphql.kickstart.tools.GraphQLMutationResolver;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.studentmanagementservice.domain1.model.Student;
import com.studentmanagementservice.persistence.jpa.entity.StudentEntity;
import com.studentmanagementservice.persistence.jpa.repository.StudentRepository;
@Component
public class Mutation implements GraphQLMutationResolver {
	private static Logger logger = LoggerFactory.getLogger(Mutation.class);
	private StudentRepository studentRepository;
	
	@Autowired
	public Mutation(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	public StudentEntity addStudent(String name,String qualification,String city) {
		//Student Sstudent=new Student();
		StudentEntity student=new StudentEntity();
		student.setName(name);
		student.setQualification(qualification);
		student.setCity(city);
		studentRepository.save(student);
		logger.info("Adding student "+student);
		//BeanUtils.copyProperties(Sstudent, student);
		return student;
	}
	
	public StudentEntity updateStudent(int studentId, String name, String qualification,String city) throws UserNotFoundException {
		Optional<StudentEntity> updateStudent=studentRepository.findById(studentId);
		if(updateStudent.isPresent()) {
			//Student Sstudent=new Student();
			StudentEntity student=updateStudent.get();
			if(name!=null) {
				student.setName(name);
			}
			if(qualification!=null) {
				student.setQualification(qualification);
			}
			if(city!=null) {
				student.setCity(city);
			}
			studentRepository.save(student);
			logger.info("Updating student "+student);
			//BeanUtils.copyProperties(Sstudent, student);
			return student;	
		}
		throw new UserNotFoundException("Student ID" + studentId + " not found");
	}
	
	public String deleteStudent(int studentId) {
		logger.info("Deleting student ID "+studentId);
		studentRepository.deleteById(studentId);
		return "Student ID "+studentId+" deleted";
	}
	
	public class UserNotFoundException extends Exception {  
	    public UserNotFoundException(String errorMessage) {  
	    super(errorMessage);  
	    }  
	}
	
	
}
