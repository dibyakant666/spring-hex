package com.studentmanagementservice.infrastructure.persistance.inmemory.adapter;

import java.util.*;

import com.studentmanagementservice.domain1.model.Student;
import com.studentmanagementservice.domain1.spi.StudentPersistencePort;

public class StudentInMemoryAdapter implements StudentPersistencePort {
	private static final Map<Integer, Student> studentMap = new HashMap<Integer, Student>(0);

    public void addStudent(Student student) {
        studentMap.put(student.getStudentId(), student);
    }

    public void removeStudent(Student student) {
        studentMap.remove(student.getStudentId());
    }

    public List<Student> getStudents() {
        return new ArrayList<Student>(studentMap.values());
    }

    public Student getStudentById(int id) {
        return studentMap.get(id);
    }

	
}
