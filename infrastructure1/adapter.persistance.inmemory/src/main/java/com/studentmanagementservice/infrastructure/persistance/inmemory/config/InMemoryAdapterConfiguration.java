package com.studentmanagementservice.infrastructure.persistance.inmemory.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.studentmanagementservice.domain1.spi.StudentPersistencePort;
import com.studentmanagementservice.infrastructure.persistance.inmemory.adapter.StudentInMemoryAdapter;

@Configuration
public class InMemoryAdapterConfiguration {
	@Bean
    public StudentPersistencePort getStudentPersistencePort() {
        return new StudentInMemoryAdapter();
    }
}
