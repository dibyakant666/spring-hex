package com.studentmanagementservice.domain1.model;

public class Student {
	private int studentId;
	private String name;
	private String qualification;
	private String city;
	public Student() {
	}
	public Student(int studentId, String name, String qualification, String city) {
		super();
		this.studentId = studentId;
		this.name = name;
		this.qualification = qualification;
		this.city = city;
	}
	
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
}
