package com.studentmanagementservice.app.serviceconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.studentmanagementservice.app.service.adapter.StudentServiceAdapter;
import com.studentmanagementservice.app.service.api.StudentService;
import com.studentmanagementservice.domain1.spi.StudentPersistencePort;
@Configuration
public class ApplicationConfiguration {
@Bean
public StudentService getStudentService(StudentPersistencePort studentPersistencePort){
return new StudentServiceAdapter(studentPersistencePort);
}
}
