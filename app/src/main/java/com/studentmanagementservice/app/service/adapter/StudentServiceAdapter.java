package com.studentmanagementservice.app.service.adapter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.studentmanagementservice.app.service.api.StudentService;
import com.studentmanagementservice.domain1.model.Student;
import com.studentmanagementservice.domain1.spi.StudentPersistencePort;



public class StudentServiceAdapter implements StudentService {
	private StudentPersistencePort studentPersistencePort;
	@Autowired
	public StudentServiceAdapter(StudentPersistencePort studentPersistencePort) {
		this.studentPersistencePort=studentPersistencePort;
	}
	@Override
	public void addStudent(Student student) {
		studentPersistencePort.addStudent(student);
		
	}
	@Override
    public void removeStudent(Student student) {
    	studentPersistencePort.removeStudent(student);
    }
	@Override
    public List<Student> getStudents(){
		return studentPersistencePort.getStudents();
	}

   public Student getStudentById(int studentId) {
   return studentPersistencePort.getStudentById(studentId);

    }

}
