package com.studentmanagementservice.app.service.api;


import java.util.List;

import com.studentmanagementservice.domain1.model.Student;

public interface StudentService {
	void addStudent(Student student);

    void removeStudent(Student student);

    List<Student> getStudents();

    Student getStudentById(int studentId);
}
